/*******
Sample script for creating and populating tables for Assignment 2, ISYS224, 2018
*******/

/**
Drop old Tables
**/
DROP TABLE IF EXISTS T_Repayment;
DROP TABLE IF EXISTS T_Loan;
DROP TABLE IF EXISTS T_Own;
DROP TABLE IF EXISTS T_Customer;

DROP TABLE IF EXISTS T_Account;
DROP TABLE IF EXISTS T_Loan_Type;
DROP TABLE IF EXISTS T_Acc_Type;

/**
Create Tables
**/

-- Customer --
CREATE TABLE T_Customer (
  CustomerID VARCHAR(10) NOT NULL,
  CustomerName VARCHAR(45) NULL,
  CustomerAddress VARCHAR(45) NULL,
  CustomerContactNo INT NULL,
  CustomerEmail VARCHAR(45) NULL,
  CustomerJoinDate DATETIME NULL,
  PRIMARY KEY (CustomerID));

-- Acc_Type --

CREATE TABLE IF NOT EXISTS T_Acc_Type (
  AccountTypeID VARCHAR(10) NOT NULL,
  TypeName SET('SAV','CHK','LON'),
  TypeDesc VARCHAR(45) NULL,
  TypeRate DECIMAL(4,2) NULL,
  TypeFee DECIMAL(2) NULL,
  PRIMARY KEY (AccountTypeID));
  
-- Account --

CREATE TABLE IF NOT EXISTS T_Account (
  BSB VARCHAR(10) NOT NULL,
  AccountNo VARCHAR(10) NOT NULL,
  AccountBal DECIMAL(10) NULL,
  AccountType VARCHAR(10) NOT NULL,
  PRIMARY KEY (BSB, AccountNo),
    FOREIGN KEY (AccountType)
    REFERENCES T_Acc_Type(AccountTypeID));


-- Loan_Type --

CREATE TABLE IF NOT EXISTS T_Loan_Type (
  LoanTypeID VARCHAR(10) NOT NULL,
  Loan_TypeName SET('HL','IL','PL'),
  Loan_TypeDesc VARCHAR(45) NULL,
  Loan_TypeMInRate DECIMAL(4,2) NULL,
  PRIMARY KEY (LoanTypeID));
  
-- Loan --

CREATE TABLE IF NOT EXISTS T_Loan (
  LoanID VARCHAR(10) NOT NULL,
  LoanRate DECIMAL(4,2) NULL,
  LoanAmount DECIMAL(8) NULL,
  Loan_Type VARCHAR(10) NOT NULL,
  Loan_AccountBSB VARCHAR(10) NOT NULL,
  Loan_AcctNo VARCHAR(10) NOT NULL,
  PRIMARY KEY (LoanID),
	FOREIGN KEY (Loan_Type)
    REFERENCES T_Loan_Type (LoanTypeID),
    FOREIGN KEY (Loan_AccountBSB , Loan_AcctNo)
    REFERENCES T_Account (BSB, AccountNo));

-- Repayment --

CREATE TABLE IF NOT EXISTS T_Repayment (
  RepaymentNo int NOT NULL AUTO_INCREMENT,
  Repayment_LoanID VARCHAR(10) NOT NULL,
  RepaymentAmount DECIMAL(6) NULL,
  RepaymentDate DATETIME NULL,
  PRIMARY KEY (RepaymentNo),
    FOREIGN KEY (Repayment_LoanID)
    REFERENCES T_Loan (LoanID));

-- Own --

CREATE TABLE IF NOT EXISTS T_Own (
  Customer_ID VARCHAR(10) NOT NULL,
  Account_BSB VARCHAR(10) NOT NULL,
  Account_No VARCHAR(10) NOT NULL,
  PRIMARY KEY (Customer_ID, Account_BSB, Account_No),
    FOREIGN KEY (Customer_ID)
    REFERENCES T_Customer (customerID),
    FOREIGN KEY (Account_BSB, Account_No)
    REFERENCES T_Account (BSB, AccountNo));
       


/* 
Populate Tables
*/




INSERT INTO T_Customer VALUES ('C1','Adam','AdamHouse','234567891','aMail','2015-10-10');
INSERT INTO T_Customer VALUES ('C2','Badshah','BadshahPalace','234567892','bMail','2015-10-11');
INSERT INTO T_Customer VALUES ('C3','Chandni','ChandniBar','234567893','cMail','2015-10-12');

INSERT INTO T_Acc_Type VALUES ('AT1','SAV','Savings','0.1','15');
INSERT INTO T_Acc_Type VALUES ('AT2','CHK','Checking','0.2','16');
INSERT INTO T_Acc_Type VALUES ('AT3','LON','Loan','0','17');

INSERT INTO T_Account VALUES ('BSB1','Acct1','10.00','AT1');
INSERT INTO T_Account VALUES ('BSB2','Acct2','11.00','AT3');
INSERT INTO T_Account VALUES ('BSB3','Acct3','-5000','AT3');
INSERT INTO T_Account VALUES ('BSB3','Acct4','-7000','AT3');
INSERT INTO T_Account VALUES ('BSB1','Acct5','10.00','AT1');
INSERT INTO T_Account VALUES ('BSB1','Acct6','10.00','AT1');

INSERT INTO T_Loan_Type VALUES ('LT1','HL','Home Loan','0.01');
INSERT INTO T_Loan_Type VALUES ('LT2','IL','Investment Loan','0.02');
INSERT INTO T_Loan_Type VALUES ('LT3','PL','Personal Loan','0.03');

INSERT INTO T_Loan VALUES ('L1','0.05','5000.00','LT3','BSB3','Acct4');
INSERT INTO T_Loan VALUES ('L2','0.02','16200.00','LT2','BSB2','Acct2');
INSERT INTO T_Loan VALUES ('L3','0.03','670500.00','LT1','BSB3','Acct3');

INSERT INTO T_Repayment (Repayment_LoanID, RepaymentAmount, RepaymentDate)
       	VALUES ('L1','1.00','2017-10-10');
INSERT INTO T_Repayment  (Repayment_LoanID, RepaymentAmount, RepaymentDate)
        VALUES ('L2','2.00','2018-02-11');
INSERT INTO T_Repayment  (Repayment_LoanID, RepaymentAmount, RepaymentDate)
        VALUES ('L3','2.00','2018-02-11');

INSERT INTO T_Own VALUES ('C1','BSB2','Acct2');
INSERT INTO T_Own VALUES ('C2','BSB3','Acct3');
INSERT INTO T_Own VALUES ('C3','BSB3','Acct4');
INSERT INTO T_Own VALUES ('C1','BSB3','Acct4');
INSERT INTO T_Own VALUES ('C1','BSB1','Acct1');
INSERT INTO T_Own VALUES ('C2','BSB1','Acct5');
INSERT INTO T_Own VALUES ('C3','BSB1','Acct6');

/**
End OF GIVEN Script
**/

/*
TASK 2
*/



DROP PROCEDURE IF EXISTS REPAY_LOAN;

DELIMITER //

CREATE PROCEDURE REPAY_LOAN(
		IN from_bsb VARCHAR(10), 
        IN from_account_no VARCHAR(10), 
        IN to_loan VARCHAR(10), 
        IN amount DECIMAL(6,0)
        )
	BEGIN
		DECLARE loan_account_no, loan_account_bsb, loan_customer_id, loop_customer_id VARCHAR(10) DEFAULT '';
        DECLARE loan_amount, account_balance DECIMAL(6,0) DEFAULT 0;
        DECLARE v_finished, final_execution_state INT DEFAULT FALSE;
        DECLARE error_message VARCHAR (64) DEFAULT '';
        DECLARE customer_cursor CURSOR FOR 
			SELECT o.Customer_ID 
			FROM t_loan l, t_own o 
            WHERE l.LoanID = to_loan 
            AND l.Loan_AcctNo = o.Account_No 
            AND l.Loan_AccountBSB = o.Account_BSB;
        
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = TRUE;
        
        #CHECK if accountNum and accountBSB belongs to the same customer
                OPEN customer_cursor;
					cursorLoop : Loop
						FETCH customer_cursor INTO loan_customer_id ;
							IF v_finished THEN          
								LEAVE cursorLoop;
							END IF;
                            
                            #SEARCH account details related to the customer 
							SELECT Customer_ID 
									INTO loop_customer_id 
									FROM t_own 
                                    WHERE  
									Account_BSB = from_bsb
									AND 
									Account_No = from_account_no;	
							#CHECK if customers are matching
                            IF(loop_customer_id = loan_customer_id) THEN    
								SET final_execution_state = TRUE;
                                LEAVE cursorLoop;
							END IF;
						END Loop cursorLoop; 
                      
                      IF (final_execution_state = FALSE) THEN 
						SET error_message = 'Accounts do not match to the customer';
                      END IF;
                      
                CLOSE customer_cursor;
      
			#CHECK Amount in the Account
            IF(final_execution_state) THEN
				#CHECK if the fund is Sufficient or Not
                SELECT AccountBal INTO account_balance FROM t_account WHERE BSB = from_bsb AND AccountNo = from_account_no;
					IF (account_balance <= 0 OR account_balance < amount )THEN
						SET final_execution_state = FALSE;
                        SET error_message = 'Account does not have enough balance';
                    END IF;
            END IF;        
			
            #UPDATE records
            IF(final_execution_state) THEN
            
				#FETCH account details of loan accounts to 
				SELECT a.AccountNo, a.BSB INTO loan_account_no, loan_account_bsb FROM t_account a, t_loan l
					WHERE a.AccountNo = l.Loan_AcctNo 
                    AND l.LoanID = to_loan;
                    
                #INSERT Repayment
                INSERT INTO t_repayment (Repayment_LoanID, RepaymentAmount, RepaymentDate) VALUES (to_loan, amount, now());
                
                #DEDUCT amount from the origin account
				UPDATE t_account SET AccountBal = (AccountBal-amount) WHERE AccountNo = from_account_no AND BSB = from_bsb; 
                
                #DEDUCT amount from loan amount in the loan account
				UPDATE t_account SET AccountBal = (AccountBal+amount) WHERE AccountNo = loan_account_no AND BSB = loan_account_bsb;
                
                SELECT 'Operation Successful' message;
			ELSE 
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT =  error_message;
            END IF;
		END//
DELIMITER ;


/*
TASK 3

*/
/*TEST data populting
*/
INSERT INTO t_account (AccountNo, BSB, AccountBal, AccountType)
VALUES('Acct6', 'BSB6', -40000, 'AT3'),
		('Acct7', 'BSB7', -40000, 'AT3'),
		('Acct8', 'BSB8', -40000, 'AT3'),
        ('Acct9', 'BSB9', -40000, 'AT3'),
        ('Acct10', 'BSB10', -40000, 'AT3'),
        ('Acct11', 'BSB11', -40000, 'AT3'),
        ('Acct12', 'BSB12', -40000, 'AT3'),
        ('Acct13', 'BSB13', -40000, 'AT3'),
		('Acct14', 'BSB14', -40000, 'AT3');
        
INSERT INTO t_own (Account_No, Account_BSB, Customer_ID) 
	values ('Acct7', 'BSB7', 'C1'),
        ('Acct8', 'BSB8', 'C1'),
        ('Acct9', 'BSB9', 'C1'),
        ('Acct10', 'BSB10','C1'),
        ('Acct11', 'BSB11','C1'),
        ('Acct12', 'BSB12','C1'),
        ('Acct13', 'BSB13','C1'),
        ('Acct14', 'BSB14','C1');


DROP TRIGGER IF EXISTS VALIDATE_NEW_LOAN;

DELIMITER //

CREATE TRIGGER VALIDATE_NEW_LOAN
BEFORE INSERT 
ON t_loan FOR EACH row
BEGIN 
		DECLARE  customer_cursor_done, number_of_individual_loan, number_of_joint_loan, number_of_total_loan, number_of_personal_loan, number_of_home_loan INT;
		DECLARE  total_loan_amount DECIMAL(8, 0);
        DECLARE  message longtext;
		DECLARE account_no, account_bsb, account_type, customer_id varchar (10);
     
		DECLARE CUSTOMER_CURSOR CURSOR FOR 
			
            SELECT DISTINCT o.Customer_ID, a.AccountType 
			FROM t_own o , t_loan l,  t_account a
            WHERE o.Account_No = NEW.Loan_AcctNo
            AND o.Account_BSB = NEW.Loan_AccountBSB
            AND o.Account_No = a.AccountNo 
            AND o.Account_BSB = a.BSB;
            
            
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET customer_cursor_done = TRUE;
        
			#CHECK if accountNum and accountBSB belongs to the same customer
            OPEN CUSTOMER_CURSOR;
					cursorLoop : Loop
						FETCH CUSTOMER_CURSOR INTO customer_id, account_type ;
							IF customer_cursor_done THEN          
								LEAVE cursorLoop;
							END IF;
     
			
			#number of total_loans
			SELECT count(*) INTO number_of_total_loan FROM t_own o, t_account a
				WHERE o.Customer_ID = customer_id
				AND a.AccountNo = o.Account_No
				AND a.BSB = o.Account_BSB
				AND a.AccountType = 'AT3'
				AND a.AccountBal < 0; #Loan is not yet paid
			
			IF number_of_total_loan >= 8 THEN
				SET message = concat('Maximum number of total loans(8) for ', customer_id,' reached');
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = message;
			#number_of_individual_loan
			ELSE	    
			SELECT count(*) INTO number_of_joint_loan FROM (
				SELECT o.Customer_ID, o.Account_No, o.Account_BSB FROM t_own o , t_account a
					WHERE
					o.Account_No IN (SELECT Account_No FROM t_own
					GROUP BY Account_No, Account_BSB
					HAVING count(*) > 1) 
					AND o.Account_BSB IN (SELECT Account_BSB FROM t_own
					GROUP BY Account_No, Account_BSB
					HAVING count(*) > 1)     
					AND o.Customer_ID = customer_id			
					AND a.AccountNo = o.Account_No
					AND a.BSB = o.Account_BSB
					AND a.AccountType = 'AT3' #account type loan
					AND a.AccountBal < 0) as temporary_table;
			
					 IF (number_of_total_loan - number_of_joint_loan) >= 5 THEN
						SET message = concat('Maximum number of total individual loans(5) for ', customer_id,' reached');
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = message;
					ELSE
					#number of PERSONAL LOAN
					SELECT count(*) INTO number_of_personal_loan FROM t_own o, t_account a, t_loan l
						WHERE o.Customer_ID = customer_id
						AND a.AccountType = 'AT3' #Loan Account
						AND a.AccountNo = o.Account_No		
						AND a.BSB = o.Account_BSB
						 AND o.Account_No = l.Loan_AcctNo
						AND o.Account_BSB = l.Loan_AccountBSB
						AND l.Loan_Type = 'LT3' #Personal loan type
						;
			  
						IF number_of_personal_loan >= 1 THEN
								SET message = concat('Maximum number of  Personal Loans(1) for ', customer_id,' reached');
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = message;					
						ELSE
						#number of HOME LOAN
						SELECT count(*) INTO number_of_home_loan FROM t_own o, t_account a, t_loan l
							WHERE o.Customer_ID = customer_id
							AND a.AccountType = 'AT3' #Loan Account
							AND a.AccountNo = o.Account_No
							AND a.BSB = o.Account_BSB
							 AND o.Account_No = l.Loan_AcctNo
							AND o.Account_BSB = l.Loan_AccountBSB
							AND l.Loan_Type = 'LT1' #Home loan type
							;
					  
						IF number_of_home_loan >= 1 THEN
								SET message = concat('Maximum number of  Home Loans(3) for ', customer_id,' reached');
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT =  message;					
						ELSE
						#number of Total Loan not exceeding 10M
						SELECT sum(l.LoanAmount) INTO total_loan_amount FROM t_own o, t_account a, t_loan l
							WHERE o.Customer_ID = customer_id
							AND a.AccountType = 'AT3'
							AND a.AccountBal < 0
							AND a.AccountNo = o.Account_No
							AND a.BSB = o.Account_BSB
							AND o.Account_No = l.Loan_AcctNo
							AND o.Account_BSB = l.Loan_AccountBSB;
			  
						IF total_loan_amount  >= 10000000 THEN
								SET message = concat('Maximum  amount of Loan(10,000,000) for ', customer_id,' reached');
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT =  message;	
			END IF;
        END IF;
        END IF;
        END IF;
        END IF;
        END LOOP cursorLoop;
        CLOSE CUSTOMER_CURSOR;
END //

DELIMITER ;


/*
TASK 4
*/

#ALTER TABLE TO ADD COLUMNS FOR SAVING OFFSET ACCOUNT DETAILS
ALTER TABLE t_loan 
ADD COLUMN offset_account_id VARCHAR(10) NULL,
ADD COLUMN offset_account_bsb VARCHAR(10)  NULL;

#create offset log
CREATE TABLE Offset_Log (
	offset_log_id INT auto_increment primary key,
    offset_account_no varchar (10),
    offset_account_bsb varchar (10),
    offset_account_log_date datetime,
    offset_account_balance decimal(6, 2)
    );


#ADDING SAMPLE LINK TO A LOAN 'L1'
UPDATE t_loan SET offset_account_id = 'BSB2', offset_account_bsb = 'Acct2' 
	WHERE Loan_AccountBSB = 'BSB3' AND Loan_AcctNo = 'Acct4';
    
INSERT INTO t_account 
VALUES('Acct20', 'BSB20', 40000, 'AT1');    


DROP PROCEDURE IF EXISTS CALCULATE_INTEREST;

DELIMITER //

CREATE PROCEDURE CALCULATE_INTEREST(
	IN input_date DATE
)
	BEGIN
		DECLARE result, loan_interest LONGTEXT DEFAULT '';
		DECLARE loan_id, offset_account_id, offset_account_bsb VARCHAR(10);
        DECLARE repayment_no, number_of_days,loan_cursor_done, repayment_cursor_done INTEGER;
        DECLARE repayment_amount, principal, rate, interest, cumulative_interest, offset_account_balance, remaining_loan_amount DECIMAL (6,2) DEFAULT 0.00;
        DECLARE last_date, repayment_date  DATETIME;
		
        
        DECLARE loan_cursor CURSOR FOR 
			SELECT l.loanID, LoanRate, l.offset_account_id, l.offset_account_bsb FROM t_loan l, t_account a 
				WHERE a.accountBal < 0 
				AND a.AccountNo = l.Loan_AcctNo 
				AND a.BSB = l.Loan_AccountBSB;
		
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET loan_cursor_done = 1;
        
        #GET previous months 25th
        SET last_date =  input_date - INTERVAL 1 MONTH;
        
        #LOOP loans
		OPEN loan_cursor;	
        	LOAN_CURSOR_LOOP: LOOP
			FETCH loan_cursor INTO loan_id, rate, offset_account_id, offset_account_bsb;
				IF loan_cursor_done THEN
					LEAVE LOAN_CURSOR_LOOP;
				END IF;
            
                #LOOP Repayments for Loan
				REPAYMENTS:BEGIN					
					DECLARE repayment_cursor CURSOR FOR
						SELECT RepaymentAmount, RepaymentDate
							FROM t_repayment 
							WHERE Repayment_LoanID = loan_id 
                            AND RepaymentDate BETWEEN last_date AND input_date
							ORDER BY RepaymentDate ASC;
					
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET repayment_cursor_done = 1;
                    
					OPEN repayment_cursor;
						REPAYMENT_CURSOR_LOOP:LOOP
                        FETCH repayment_cursor INTO repayment_amount, repayment_date;
								IF repayment_cursor_done THEN
									LEAVE REPAYMENT_CURSOR_LOOP;
								END IF;	
                                
                                #get offset details
                                SELECT offset_account_balance INTO offset_account_balance FROM Offset_Log
									WHERE offset_account_log_date = repayment_date
									AND offset_account_no = offset_account_id
                                    AND offset_account_bsb = offset_account_bsb;
                                
								SET principal = offset_account_balance - repayment_amount;
								
								#CALCULATE number of days
								SET number_of_days = DATEDIFF(last_date, repayment_date);
			   
								SET last_date = repayment_date;
							   
								#RATE from loan details
								SET interest = (principal * number_of_days * rate)/(365);
								
								SET cumulative_interest = cumulative_interest + interest;
								
                        END LOOP REPAYMENT_CURSOR_LOOP;
                        
                        SET loan_interest = CONCAT(cumulative_interest, ' | ' );
                  
					CLOSE repayment_cursor;
                                    
                END REPAYMENTS;
                #RESET VALUES FOR NEXT LOAN IN LOOP
				SET result = CONCAT(result , loan_id, ':' , loan_interest);
				SET cumulative_interest = 0.00;
				SET principal = 0.00;
				SET last_date =  input_date - INTERVAL 1 MONTH;
    
			END LOOP LOAN_CURSOR_LOOP;
                      
        CLOSE loan_cursor;
        
	SELECT result as 'Loan_id: interest';
        
        
	END//

DELIMITER ;